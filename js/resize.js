function autoResizing(){

	var heightMenuCategoryLabel = 0;
	var heightFoodDesc = 0;
	var heightFoodPricing = 0;


	var heightEtitle =0;
	var heightEventMeta =0;
	var heightBtnEvent =0;
	var heightEtext =0;


	//	Initiating all heights to their default values

	$('.my-menu-category-label').css({'height': 'auto'});

	$('li.my-content').each(function(){
		$(this).find('.fh5co-food-desc').css({'height': 'auto'});
		$(this).find('.fh5co-food-pricing').css({'height': 'auto'});
	});

	$('.event-menu').each(function(){
		$(this).find('.etitle').css({'height': 'auto'});
		$(this).find('.fh5co-event-meta').css({'height': 'auto'});
		$(this).find('.btn-event').css({'height': 'auto'});
		$(this).find('.etext').css({'height': 'auto'});
	});


	//	Resize only if the screen is larger than 992px

	if(!Modernizr.mq('(min-width: 992px)'))
		return;


	//	Determining, which respective element has the greatest size

	$('.my-menu-category-label').each(function(){

		var menuCategoryLabelHeight = parseInt($(this).css('height'), 10);

		if(menuCategoryLabelHeight > heightMenuCategoryLabel)
			heightMenuCategoryLabel = menuCategoryLabelHeight;

	});

	
	$('li.my-content').each(function(){

		var fh5coFoodDescHeight = parseInt($(this).find('.fh5co-food-desc').css('height'), 10);
		var fh5coFoodPricingHeight = parseInt($(this).find('.fh5co-food-pricing').css('height'), 10);

		if(fh5coFoodDescHeight > heightFoodDesc)
			heightFoodDesc = fh5coFoodDescHeight;

		if(fh5coFoodPricingHeight > heightFoodPricing)
			heightFoodPricing = fh5coFoodPricingHeight;

	});
	$('.event-menu').each(function(){
		var etitleHeight = parseInt($(this).find('.etitle').css('height'), 10);
		var fh5coEventMetaHeight = parseInt($(this).find('.fh5co-event-meta').css('height'), 10);
		var btnEventHeight = parseInt($(this).find('.btn-event').css('height'), 10);
		var etextHeight =parseInt($(this).find('.etext').css('height'), 10);
		
		if (etitleHeight > heightEtitle)
			heightEtitle = etitleHeight;
		
		if (fh5coEventMetaHeight > heightEventMeta)
			heightEventMeta = fh5coEventMetaHeight;
		
		if (btnEventHeight > heightBtnEvent)
			heightBtnEvent = btnEventHeight;

		if (etextHeight > heightEtext) 
			heightEtext = etextHeight;
	});

	
	//	Each respective element gets the value corresponding to the greatest size

	$('.my-menu-category-label').css({'height': heightMenuCategoryLabel});

	$('li.my-content').each(function(){
		$(this).find('.fh5co-food-desc').css({'height': heightFoodDesc});
		$(this).find('.fh5co-food-pricing').css({'height': heightFoodPricing});
	});

	$('.event-menu').each(function(){
		$(this).find('.etitle').css({'height': heightEtitle});
		$(this).find('.fh5co-event-meta').css({'height': heightEventMeta});
		$(this).find('.etext').css({'height': heightEtext});
		$(this).find('.btn-event').css({'height': heightBtnEvent})
	});

}
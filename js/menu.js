$(function(){

	$('.my-content').click(function () {

		if ($(this).find('h3').text() != $('#get-content').find('h3').text()){

			desc = $(this).find('span.desc').text();
			title = $(this).find('h3').text();
			img = $(this).find('img').attr('src');
			price = $(this).find('.fh5co-food-pricing').text();

			$('#get-content-image').attr('src',img);
			$('#get-content-desc').text(desc);
			$('#get-content-title').text(title);
			$('#get-content-price').text(price);

			$('#get-content').slideDown(180);
			$('html, body').animate({scrollTop: $('#get-content').offset().top}, 180);


		} else {

			$('#get-content').slideUp(180);
			$('#get-content').find('h3').text('');

		}
		
	});

});